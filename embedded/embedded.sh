#!/bin/bash
export ROS_MASTER_URI=http://192.168.100.100:11311
export ROS_HOSTNAME=192.168.100.100s
export PATH=$PATH:~/.platformio/penv/bin

source /opt/ros/kinetic/setup.bash  
source ~/drivers_ws/devel/setup.bash 
source ~/ant_ws/devel/setup.bash 

# xterm -hold -e  "roslaunch build_embedded mcu.launch"
# sleep 1
# xterm -hold -e  "roslaunch rosserial_server socket.launch"


