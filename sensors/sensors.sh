#!/bin/bash
sleep 30
export ROS_MASTER_URI=http://192.168.100.100:11311
export ROS_HOSTNAME=192.168.100.100

source /opt/ros/kinetic/setup.bash  
source ~/drivers_ws/devel/setup.bash 
source ~/ant_ws/devel/setup.bash 

xterm -hold -e  "roslaunch system_behaviour system_initializer.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_description spawn.launch" &
sleep 5 
xterm -hold -e  "roslaunch build_embedded mcu.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_control rs_t265.launch" &
sleep 5
# xterm -hold -e  "roslaunch realsense2_camera rs_camera.launch camera:=d435 serial_no:=827312073651 filters:=pointcloud enable_infra1:=false enable_infra2:=false" &
# sleep 5
xterm -hold -e  "roslaunch astra_camera  astrapro.launch serial_no:=17091100033" &
sleep 5
xterm -hold -e  "roslaunch rplidar_ros rplidar.launch" &
sleep 5
xterm -hold -e  "roslaunch intel_realsense rs_rgbd.launch" &
sleep 5
xterm -hold -e  "roslaunch intel_realsense rs_rgbd_2.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_control laser_filter.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_control laser_merger.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_control depth_to_laserscan.launch" &
sleep 5
# xterm -hold -e  "roslaunch rs_odom_fix rs_odom_fix_composite.launch" &
# sleep 5 
# xterm -hold -e  "roslaunch imu_odom imu_odom.launch" &
# xterm -hold -e  "roslaunch ants_navigation rtab_map.launch" &
# sleep 5
xterm -hold -e  "roslaunch ants_control ekf_localization.launch" &
sleep 5
xterm -hold -e  "roslaunch ants_navigation amcl_demo.launch"  
