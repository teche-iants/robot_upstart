#!/bin/bash
sleep 35
export ROS_MASTER_URI=http://192.168.100.100:11311
export ROS_HOSTNAME=192.168.100.100

source /opt/ros/kinetic/setup.bash  
source ~/drivers_ws/devel/setup.bash 
source ~/ant_ws/devel/setup.bash 

xterm -hold -e  "ds4drv" &
sleep 5
xterm -hold -e  "roslaunch ants_control teleop.launch" &
sleep 5
xterm -hold -e  "rosrun topic_tools throttle messages cmd_vel1 30.0 cmd_vel" &
sleep 5
# xterm -hold -e  "rosrun base_state_updater state_updater_node" &
# sleep 5
xterm -hold -e  "rosrun base_inv_kinematics inv_kinematics_node" &
sleep 5
xterm -hold -e  "roslaunch ants_control wheel_pid.launch"




